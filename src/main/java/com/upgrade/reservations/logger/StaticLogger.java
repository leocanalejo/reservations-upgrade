package com.upgrade.reservations.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StaticLogger {

    private static final Logger logger = LoggerFactory.getLogger("Reservation Logger");

    public static void info(String log){
        logger.info(log);
    }

    public static void error(String log){
        logger.error(log);
    }

    public static void debug(String log){
        logger.debug(log);
    }

    public static void trace(String log){
        logger.trace(log);
    }

    public static void warning(String log){
        logger.warn(log);
    }

    public static void info(String log, Exception e){
        logger.info(log, e);
    }

    public static void error(String log, Exception e){
        logger.error(log, e);
    }

    public static void debug(String log, Exception e){
        logger.debug(log, e);
    }

    public static void trace(String log, Exception e){
        logger.trace(log, e);
    }

    public static void warning(String log, Exception e){
        logger.warn(log, e);
    }

}