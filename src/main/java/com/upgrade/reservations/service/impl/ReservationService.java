package com.upgrade.reservations.service.impl;

import com.upgrade.reservations.exception.BadRequestException;
import com.upgrade.reservations.exception.InternalServerErrorException;
import com.upgrade.reservations.exception.NotFoundException;
import com.upgrade.reservations.model.Reservation;
import com.upgrade.reservations.model.DayAvailability;
import com.upgrade.reservations.repository.DayAvailabilityRepository;
import com.upgrade.reservations.repository.ReservationRepository;
import com.upgrade.reservations.service.IReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

@Service
public class ReservationService implements IReservationService {

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    DayAvailabilityRepository dayAvailabilityRepository;

    @Override
    @Transactional(readOnly = true)
    public Reservation getReservationById(Long reservationId) {
        try {
            return reservationRepository.findById(reservationId).orElseThrow(
                    () -> new NotFoundException("No reservation found with id " + reservationId)
            );
        } catch (DataAccessException e) {
            throw new InternalServerErrorException("Error accessing database");
        }
    }

    @Override
    @Transactional
    public Long reserve(Reservation reservation) {
        validateDates(reservation);
        checkDaysAvailability(reservation);
        try {
            return saveReserve(reservation);
        } catch (DataAccessException e) {
            throw new InternalServerErrorException("Error accessing database");
        }
    }

    @Override
    @Transactional
    public void cancelReservationById(Long reservationId) {
        try {
            addAvailableDays(reservationId);
            reservationRepository.deleteById(reservationId);
        } catch (DataAccessException e) {
            throw new InternalServerErrorException("Error accessing database");
        }

    }

    @Override
    @Transactional
    public Long updateReservation(Reservation reservation, Long reservationId) {
        try {
            addAvailableDays(reservationId);
            Reservation existingReservation = reservationRepository.findById(reservationId).orElseThrow(
                    () -> new NotFoundException("No reservation found with id " + reservationId)
            );
            existingReservation.setFullName(reservation.getFullName());
            existingReservation.setEmail(reservation.getEmail());
            existingReservation.setArrivalDate(reservation.getArrivalDate());
            existingReservation.setDepartureDate(reservation.getDepartureDate());

            return reserve(existingReservation);
        } catch (DataAccessException e) {
            throw new InternalServerErrorException("Error accessing database");
        }

    }

    private void validateDates(Reservation reservation) {
        LocalDate arrivalDate = reservation.getArrivalDate();
        LocalDate departureDate = reservation.getDepartureDate();

        if (Period.between(arrivalDate, departureDate).getDays() > 3) {
            throw new BadRequestException("The reservation can not be for more than 3 days.");
        }

        if (!arrivalDate.isAfter(LocalDate.now())) {
            throw new BadRequestException("The reservation must be reserved minimum 1 day ahead of arrival.");
        }

        if (departureDate.isAfter(LocalDate.now().plusMonths(1))) {
            throw new BadRequestException("The reservation must be reserved up to 1 month in advance.");
        }
    }

    private void checkDaysAvailability(Reservation reservation) {
        try {
            List<DayAvailability> dayAvailabilities = dayAvailabilityRepository.findByDayBetween(reservation.getArrivalDate(),
                    reservation.getDepartureDate().minusDays(1));
            for (DayAvailability dayAvailability : dayAvailabilities) {
                if (!(dayAvailability.getDaysRemaining() > 0)) {
                    throw new BadRequestException("Not available days");
                }
            }
        } catch (DataAccessException e) {
            throw new InternalServerErrorException("Error accessing database");
        }

    }

    private Long saveReserve(Reservation reservation) {
        try {
            subtractAvailableDays(reservation);
            return reservationRepository.save(reservation).getReservationId();
        } catch (DataAccessException e) {
            throw new InternalServerErrorException("Error accessing database");
        }
    }

    private void subtractAvailableDays(Reservation reservation) {
        List<DayAvailability> dayAvailabilities = dayAvailabilityRepository.findByDayBetween(reservation.getArrivalDate(),
                reservation.getDepartureDate().minusDays(1));
        for (DayAvailability dayAvailability : dayAvailabilities) {
            dayAvailability.setDaysRemaining(dayAvailability.getDaysRemaining() - 1);
            dayAvailabilityRepository.save(dayAvailability);
        }
    }

    private void addAvailableDays(Long reservationId) {
        Reservation reservation = reservationRepository.findById(reservationId).orElseThrow(
                () -> new NotFoundException("No reservation found with id " + reservationId));
        List<DayAvailability> dayAvailabilities = dayAvailabilityRepository.findByDayBetween(reservation.getArrivalDate(),
                reservation.getDepartureDate().minusDays(1));
        for (DayAvailability dayAvailability : dayAvailabilities) {
            dayAvailability.setDaysRemaining(dayAvailability.getDaysRemaining() + 1);
            dayAvailabilityRepository.save(dayAvailability);
        }
    }
}
