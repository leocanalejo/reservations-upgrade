package com.upgrade.reservations.service.impl;

import com.upgrade.reservations.model.DayAvailability;
import com.upgrade.reservations.repository.DayAvailabilityRepository;
import com.upgrade.reservations.service.IDayAvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class DayAvailabilityService implements IDayAvailabilityService {

    @Autowired
    DayAvailabilityRepository dayAvailabilityRepository;

    @Override
    @Transactional(readOnly = true)
    public List<DayAvailability> getReservationDaysByDates(LocalDate startDate, LocalDate endDate) {
        return dayAvailabilityRepository.findByDayBetween(startDate, endDate);
    }
}
