package com.upgrade.reservations.service;

import com.upgrade.reservations.model.DayAvailability;

import java.time.LocalDate;
import java.util.List;

public interface IDayAvailabilityService {
    List<DayAvailability> getReservationDaysByDates(LocalDate startDate, LocalDate endDate);
}
