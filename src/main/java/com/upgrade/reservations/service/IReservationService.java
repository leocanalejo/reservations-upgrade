package com.upgrade.reservations.service;

import com.upgrade.reservations.model.Reservation;

public interface IReservationService {

    Long reserve(Reservation reservation);

    void cancelReservationById(Long reservation);

    Long updateReservation(Reservation reservation, Long reservationId);

    Reservation getReservationById(Long idReservation);
}
