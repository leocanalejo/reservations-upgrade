package com.upgrade.reservations.repository;

import com.upgrade.reservations.model.DayAvailability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface DayAvailabilityRepository extends JpaRepository<DayAvailability, Long> {

    List<DayAvailability> findByDayBetween(LocalDate startDate, LocalDate endDate);

}
