package com.upgrade.reservations.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "day_availability")
public class DayAvailability {

    @Id
    @Column(name = "day", nullable = false)
    private LocalDate day;

    @Column(name = "days_remaining", nullable = false)
    private Integer daysRemaining;

    public int getDaysRemaining() {
        return daysRemaining;
    }

    public void setDaysRemaining(int daysRemaining) {
        this.daysRemaining = daysRemaining;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }
}
