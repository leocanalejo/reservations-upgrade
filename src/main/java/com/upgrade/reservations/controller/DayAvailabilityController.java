package com.upgrade.reservations.controller;

import com.upgrade.reservations.aspects.annotation.LogExecutionTime;
import com.upgrade.reservations.model.DayAvailability;
import com.upgrade.reservations.service.IDayAvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/reservation-availability")
public class DayAvailabilityController {

    @Autowired
    IDayAvailabilityService dayAvailabilityService;

    @LogExecutionTime
    @GetMapping
    public List<DayAvailability> getReservationDaysByDates(
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDate) {
        return dayAvailabilityService.getReservationDaysByDates(
                startDate.orElseGet(()-> LocalDate.now()),
                endDate.orElseGet(()-> LocalDate.now().plusMonths(1)));
    }

}
