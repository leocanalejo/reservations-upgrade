package com.upgrade.reservations.controller;

import com.upgrade.reservations.aspects.annotation.LogExecutionTime;
import com.upgrade.reservations.exception.BadRequestException;
import com.upgrade.reservations.model.Reservation;
import com.upgrade.reservations.service.IReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/reserve")
public class ReservationController {

    @Autowired
    IReservationService reservationService;

    @LogExecutionTime
    @GetMapping("/{reservationId}")
    public Reservation getReservationById(@PathVariable Long reservationId) {
        return reservationService.getReservationById(reservationId);
    }

    @LogExecutionTime
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public synchronized Long reserve(@Valid @RequestBody Reservation reservation, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new BadRequestException(getBindingResultsErrors(bindingResult));
        }
        return reservationService.reserve(reservation);
    }

    @LogExecutionTime
    @DeleteMapping("/{reservationId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelReservationById(@PathVariable Long reservationId) {
        reservationService.cancelReservationById(reservationId);
    }

    @LogExecutionTime
    @PutMapping("/{reservationId}")
    @ResponseStatus(HttpStatus.CREATED)
    public synchronized Long updateReservation(@Valid @RequestBody Reservation reservation, @PathVariable Long reservationId,
                                  BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new BadRequestException(getBindingResultsErrors(bindingResult));
        }
        return reservationService.updateReservation(reservation, reservationId);
    }

    private String getBindingResultsErrors(BindingResult bindingResult) {
        StringBuilder errors = new StringBuilder();
        for (FieldError fieldError: bindingResult.getFieldErrors()){
            errors.append("The field '")
                    .append(fieldError.getField())
                    .append("' ")
                    .append(fieldError.getDefaultMessage())
                    .append(System.lineSeparator());
        }
        return errors.toString().substring(0,errors.length()-System.lineSeparator().length());
    }

}
