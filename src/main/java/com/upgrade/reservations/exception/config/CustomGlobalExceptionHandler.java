package com.upgrade.reservations.exception.config;

import com.upgrade.reservations.exception.BadRequestException;
import com.upgrade.reservations.exception.InternalServerErrorException;
import com.upgrade.reservations.exception.NotFoundException;
import com.upgrade.reservations.logger.StaticLogger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;
import java.time.LocalDateTime;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<CustomErrorResponse> handleNotFoundException(Exception e, ServletWebRequest request) {
        return error(HttpStatus.NOT_FOUND, e, request);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({InternalServerErrorException.class, SQLException.class, NullPointerException.class})
    public ResponseEntity<CustomErrorResponse> handleInternalServerErrorException(Exception e, ServletWebRequest request) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR, e, request);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<CustomErrorResponse> handleBadRequestException(Exception e, ServletWebRequest request) {
        return error(HttpStatus.BAD_REQUEST, e, request);
    }

    private ResponseEntity<CustomErrorResponse> error(HttpStatus status, Exception e, ServletWebRequest request) {
        StaticLogger.error("Exception : ", e);
        CustomErrorResponse errors = new CustomErrorResponse.Builder()
                .withTimestamp(LocalDateTime.now())
                .withStatus(status)
                .withStatusPhrase(status.getReasonPhrase())
                .withStatusCode(status.value())
                .withMessage(e.getMessage())
                .withError(e.toString())
                .withPath(request.getRequest().getRequestURI())
                .build();

        return new ResponseEntity<>(errors, status);
    }

}