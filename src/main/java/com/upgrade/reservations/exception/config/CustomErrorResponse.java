package com.upgrade.reservations.exception.config;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class CustomErrorResponse {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;
    private HttpStatus status;
    private String statusPhrase;
    private int statusCode;
    private String error;
    private String message;
    private String path;

    private CustomErrorResponse() {
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getStatusPhrase() {
        return statusPhrase;
    }

    public void setStatusPhrase(String statusPhrase) {
        this.statusPhrase = statusPhrase;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public static class Builder {

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
        private LocalDateTime timestamp;
        private HttpStatus status;
        private String statusPhrase;
        private int statusCode;
        private String error;
        private String message;
        private String path;

        public Builder withTimestamp(LocalDateTime timestamp){
            this.timestamp = timestamp;
            return this;
        }

        public Builder withStatus(HttpStatus status){
            this.status = status;
            return this;
        }

        public Builder withStatusPhrase(String statusPhrase){
            this.statusPhrase = statusPhrase;
            return this;
        }

        public Builder withStatusCode(int statusCode){
            this.statusCode = statusCode;
            return this;
        }

        public Builder withError(String error){
            this.error = error;
            return this;
        }

        public Builder withMessage(String message){
            this.message = message;
            return this;
        }

        public Builder withPath(String path){
            this.path = path;
            return this;
        }

        public CustomErrorResponse build(){
            CustomErrorResponse customErrorResponse = new CustomErrorResponse();
            customErrorResponse.setTimestamp(this.timestamp);
            customErrorResponse.setStatus(this.status);
            customErrorResponse.setStatusPhrase(this.statusPhrase);
            customErrorResponse.setStatusCode(this.statusCode);
            customErrorResponse.setMessage(this.message);
            customErrorResponse.setError(this.error);
            customErrorResponse.setPath(this.path);
            return customErrorResponse;
        }
    }

}
